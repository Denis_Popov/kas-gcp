
variable "GITLAB_TOKEN" {
  type = string
}

variable "project_path" {
  type = string
}

variable "agent_name" {
  type = string
}

variable "token_name" {
  type    = string
  default = "kas-token"
}

variable "token_description" {
  type    = string
  default = "Token for KAS Agent Authentication"
}

variable "gitlab_graphql_api_url" {
  type    = string
  default = "https://gitlab.com/api/graphql"
}

provider "graphql" {
  url = var.gitlab_graphql_api_url
  headers = {
    "Authorization" = "Bearer ${var.GITLAB_TOKEN}"
  }
}

resource "graphql_mutation" "cluster_agent" {
  compute_from_create = false

  mutation_variables = {
    "project_path" = var.project_path
    "agent_name"   = var.agent_name
  }

  read_query_variables = {
    "project_path" = var.project_path
    "agent_name"   = var.agent_name
  }

  compute_mutation_keys = {
    "id"   = "project.clusterAgent.id"
    "name" = "project.clusterAgent.name"
  }

  create_mutation = <<EOT
    mutation createAgent($project_path: ID!, $agent_name: String!) {
      createClusterAgent(input: { projectPath: $project_path, name: $agent_name }) {
        clusterAgent {
          id
          name
        }
      errors
      }
    }
EOT
  update_mutation = <<EOT
    mutation updateAgent($id: ID!, $project_path: ID!, $agent_name: String!) {
      clusterAgentDelete(input: {id: $id}) {
      errors
      }
      createClusterAgent(input: { projectPath: $project_path, name: $agent_name }) {
        clusterAgent {
          id
          name
        }
      errors
      }
    }
EOT
  delete_mutation = <<EOT
    mutation deleteAgent($id: ID!) {
      clusterAgentDelete(input: {id: $id}) {
      errors
      }
    }
EOT
  read_query      = <<EOT
    query getAgent($agent_name: String!, $project_path: ID!) {
      project(fullPath: $project_path) {
        clusterAgent(name: $agent_name) {
          id
          name
        }
      }
    }
EOT
}

resource "graphql_mutation" "agent_token" {
  compute_from_create = true

  mutation_variables = {
    "agent_id"          = graphql_mutation.cluster_agent.computed_read_operation_variables.id
    "token_name"        = var.token_name
    "token_description" = var.token_description
  }

  read_query_variables = {
    "project_path" = var.project_path
    "agent_name"   = var.agent_name
  }

  compute_mutation_keys = {
    "token_id" = "clusterAgentTokenCreate.token.id"
    "secret"   = "clusterAgentTokenCreate.secret"
  }

  create_mutation = <<EOT
    mutation createToken($agent_id: ClustersAgentID!, $token_name: String!, $token_description: String!) {
      clusterAgentTokenCreate(input: {clusterAgentId: $agent_id, description: $token_description, name: $token_name}) {
        secret
        token {
          createdAt
          id
        }
      errors
      }
    }
EOT
  update_mutation = <<EOT
    mutation updateToken($token_id: ClustersAgentTokenID!, $agent_id: ClustersAgentID!, $token_name: String!, $token_description: String!) {
      clusterAgentTokenDelete(input: {id: $token_id}) {
        errors
      }
      clusterAgentTokenCreate(input: {clusterAgentId: $agent_id, description: $token_description, name: $token_name}) {
        secret
        token {
          createdAt
          id
        }
      errors
      }
    }
EOT
  delete_mutation = <<EOT
    mutation deleteToken($token_id: ClustersAgentTokenID!) {
      clusterAgentTokenDelete(input: {id: $token_id}) {
        errors
      }
    }
EOT
  read_query      = <<EOT
    query getToken($agent_name: String!, $project_path: ID!) {
      project(fullPath: $project_path) {
        name
        id
        clusterAgent(name: $agent_name) {
          id
          tokens {
            edges {
              node {
                id
              }
            }
          }
        }
      }
    }
EOT
}

output "token_secret" {
  value     = graphql_mutation.agent_token.computed_read_operation_variables.secret
  sensitive = true
}